
/********** TASK #2 **********/

let arr1 = [1, 2, 3, 4, "apple"];
let arr2 = [2, 3, 4, 5, NaN];

let diff1 = arr1.filter(i => !arr2.includes(i));
// фильтруем уникальные эл-ты, которые есть в 1-м массиве, но нет во 2-м.

let diff2 = arr2.filter(i => !arr1.includes(i));
// фильтруем уникальные эл-ты, которые есть во 2-м массиве, но нет в 1-м.

let result = diff1.concat(diff2);
// суммируем уникальные элементы из каждого массива.

console.log (result);

/** Краткая версия решения **/

let arr1 = [1, 2, 3, 4, "apple"];
let arr2 = [2, 3, 4, 5, NaN];
let result = arr1.filter(i => !arr2.includes(i)).concat(arr2.filter(i => !arr1.includes(i)));
console.log (result);

/********** TASK #3 **********/
/** #3.1 **/
let factorialis = {
    result: function (n){

        if(n == 0 || n == 1){
            return 1;
        }else{
            return n * this.result(n-1);
        }}
};
let n = 5;
console.log(factorialis.result(n));


/** #3.2 **/
function a(ageSon, ageFather) {
    let diff = ageFather/ageSon;
    if (diff == 2) {
        console.log("Сын вдвое младше отца");
        return;
    }

    for (let step = 0; step < ageSon; ) {
        let ageDiv = (ageFather-step) / ageSon;
        if (ageDiv == 2) {
            let realAge = Math.abs(step);
            console.log(step < 0 ? `Отец будет вдвое старше через ${realAge} лет`: `Отец был вдвое старше ${realAge} лет назад`);
            return;
        }
        if (diff < 2) {
            step--;
        }else{
            step++;
        }
    }
}
a(20, 37);


/********** TASK #4 **********/
/** Создать объект калькулятор с базовыми операциями (+ - * /) на промисах **/

function calculator(num1, operation, num2) {
    return new Promise((resolved, rejected) => {
        if (operation === '+') {
            resolved(num1 + num2);
        } else if (operation === '-') {
            resolved(num1 - num2);
        } else if (operation === '*') {
            resolved(num1 * num2);
        } else if (operation === '/') {
            resolved(num1 / num2);
        } else {
            rejected('Please check the expression');
        }
    });
}

calculator(51, '+', 3)

    .then((res) => {
        console.log(`Result: ${res}`);
    })
    .catch((res) => {
        console.log(`Error: ${res}`);
    });